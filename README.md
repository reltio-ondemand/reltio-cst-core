# Reltio Core CST #

# Description #

The CST Core Project provides all required base functionality/implementation to interact with Reltio API.
And this is going to be the base project and can be used on all our Implementation for any customers/datatenants going forward.

#Supported Features:

Current List of Features supported:

* Service methods for doing any REST API call
* Service methods for doing any Reltio REST API call (Retry & token generation processes need not to be handled @ each project level)
* Generic Attribute Class (com.reltio.cst.domain.Attribute)
* File I/O Operations
* Automatic detection of File Encoding format.

##Change Log
```

  Version           : 1.5.0
  Last Updated Date : 7th June 2021
  Last Updated By   : Mallikarjuna Aakulati
  Last Changes      : Added reader and writer classes to handle csv and flat files reading from AWS S3 and to write to AWS S3

  Version           : 1.4.9
  Last Updated Date : 23rd Septemeber June 2019
  Last Updated By   : Vignesh
  Last Changes      : Refresh Token Implementation for all API calls (https://reltio.jira.com/browse/ROCS-82)
                    : Deprecated startBackgroundTokenGenerator 
                    : Deprecated stopBackgroundTokenGenerator

  Version           : 1.4.8
  Last Updated Date : 18th Septemeber June 2019
  Last Updated By   : Sanjay
  Last Changes      : Implemented Client Credentials Grant Type

  Version           : 1.4.7
  Last Updated Date : 24th June 2019
  Last Updated By   : Sanjay
  Last Changes      : Added method to add proxy details and changed the Group name

  Version           : 1.4.6
  Last Updated Date : 19th Mar 2019
  Last Updated By   : Vignesh Chandran
  Last Changes      : ROCS-1 - Addition of function waitForTasks for multithread and bug fix when password is given as empty.

  Version           : 1.4.5
  Last Updated Date : 5th Mar 2019
  Last Updated By   : Vignesh Chandran
  Last Changes      : Fix for CUST-3070, Common function written to encrypt password in the config files.

  Version           : 1.4.4
  Last Updated Date : 11th Jan 2019
  Last Updated By   : Vignesh Chandran
  Last Changes      : Fix for CUST-3049, when refresh token expires new refresh token to be obtained by passing username and password.

  Version           : 1.4.3
  Last Updated Date : 21st Nov 2018 
  Last Updated By   : Vignesh Chandran
  Last Changes      : logging framework changed to slf4j instead of log4j
  
  Version           : 1.4.2
  Last Updated Date : 9th Aug 2018 
  Last Updated By   : Vignesh Chandran
  Last Changes      : Bug Fix CUST-2988 Special Charaters usage like # and ?
                    : Tested for all the above characters, need to add the line for encoding
                              String encodedUrl = URLEncoder.encode(crosswalkValue, StandardCharsets.UTF_8.name());


  Version           : 1.4.1

```
##Contributing 
Please visit our [Contributor Covenant Code of Conduct](https://bitbucket.org/reltio-ondemand/common/src/a8e997d2547bf4df9f69bf3e7f2fcefe28d7e551/CodeOfConduct.md?at=master&fileviewer=file-view-default) to learn more about our contribution guidlines

## Licensing
```
Copyright (c) 2017 Reltio


Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
```

## Quick Start 
To learn about dependencies, building and executing the tool view our [quick start](https://bitbucket.org/reltio-ondemand/reltio-cst-core/src/master/QuickStart.md).
