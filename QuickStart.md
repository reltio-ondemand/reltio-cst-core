#How to use Jar File
Reltio API Call Usage
Find the below Code block which explains about how to use the CST core project jar file doing the Reltio API call.



```java
import com.reltio.cst.exception.handler.APICallFailureException;
import com.reltio.cst.exception.handler.GenericException;
import com.reltio.cst.exception.handler.ReltioAPICallFailureException;
import com.reltio.cst.service.ReltioAPIService;
import com.reltio.cst.service.TokenGeneratorService;
/**
 *
 * The sample Java class for using the Reltio API service implementation
 *
 */
public class ReltioAPISampleServiceImpl {
    public static void main(String[] args) {
        //Step1: Create Reltio Token Generator Service
        TokenGeneratorService tokenGeneratorService = null;
        try {
            tokenGeneratorService = new TokenGeneratorServiceImpl(
                    TokenGeneratorServiceImplTest.username,
                    TokenGeneratorServiceImplTest.password,
                    "https://auth.reltio.com/oauth/token");
        } catch (APICallFailureException e) {
            //This Exception is thrown when username/password invalid. Or if Auth server throws any exception
            //Get the Http error code & error message sent by Reltio API as below
            System.out.println("Error Code: " + e.getErrorCode() + " >>>> Error Message: " + e.getErrorResponse());
             
        } catch (GenericException e) {
            //This exception thrown when unexpected error happens like "Auth Server is down"
            //Get the exception message as below
            System.out.println(e.getExceptionMessage());
        }
         
        //If any one exception happens then abort the process as without token, we can't do any Reltio API call.
         
        //Step2: Create Reltio API service. Give the tokenGeneratorService as input to the constructor
        ReltioAPIService reltioAPIService = new SimpleReltioAPIServiceImpl(tokenGeneratorService);
         
        //Step3: Now we can do any Reltio API call & n number of times using reltioAPIService and no need to worry about the token generation and retry process
        String response = null;
        try {
            response = reltioAPIService.get("https://dev.reltio.com/reltio/api/R37ggveZezpiECn/entities/_total");
        } catch (GenericException e) {
            //This exception thrown when unexpected error happens like "Reltio Server is down/invalid input details"
            //Get the exception message as below
            System.out.println(e.getExceptionMessage());
        } catch (ReltioAPICallFailureException e) {
            //This Exception is thrown when Reltio API throws any exception
            //Get the Http error code & error message sent by Reltio API as below
            System.out.println("Error Code: " + e.getErrorCode() + " >>>> Error Message: " + e.getErrorResponse());
             
        }
        System.out.println(response);
        //The same way we can do all other API calls
    }
}
```


#File I/O Operations:
Supports both read and write implementation for Flat File and CSV Files.
Write operations are thread safe.
ReltioFileReader Interface:
Currently this interface has 4 implementing classes which are Flat File Reader, CSV File Reader, S3 Flat File Reader and S3 CSV File Reader.
Find the corresponding class details and how to use the core project for doing the file read below:

```java
package com.reltio.file;
import java.io.IOException;
public class SimpleFileReaderTest {
    public static void main(String[] args) throws IOException {
         
        //Step 1: Create Object reference for ReltioFileReader Interface
        ReltioFileReader reltioFileReader;
          
        //Step 2: Create a object based on the File Type.
            //if CSV then use below option
                //Step 2.1: Only with File Name using ReltioCSVFileReader
                reltioFileReader = new ReltioCSVFileReader("<FileName>");
                 
                //With File Name & Decoding technique like UTF-8 or any other
                reltioFileReader = new ReltioCSVFileReader("<FileName>", "UTF-8");
             
                //with CSV File Name from AWS S3 Bucket, without Decoding technique
                reltioFileReader = new ReltioS3CSVFileReader("<awsKey>","<awsSecretKey>","<awsRegion>","<bucketName>","<FileName>");
                 
                //With File Name from AWS S3 bucket & Decoding technique like UTF-8 or any other
                reltioFileReader = new ReltioS3CSVFileReader("<awsKey>","<awsSecretKey>","<awsRegion>","<bucketName>","<FileName>", "UTF-8");
             
            //ELSE if FLAT FILE then use below option
                //Step 2.1: Only with File Name. Default Delimiter is pipe
                reltioFileReader = new ReltioFlatFileReader("<FileName>");
                 
                //With File Name & Separator like pipe, dollar
                reltioFileReader = new ReltioFlatFileReader("<FileName>","||");
                 
                //With File Name, Separator & Decoding
                reltioFileReader = new ReltioFlatFileReader("<FileName>","||", "UTF-8");
         
         		//Only with File Name from AWS S3 Bucket. Default Delimiter is pipe
                reltioFileReader = new ReltioS3FlatFileReader("<awsKey>","<awsSecretKey>","<awsRegion>","<bucketName>",<FileName>");
                 
                //With File Name from AWS S3 Bucket & Separator like pipe, dollar
                reltioFileReader = new ReltioS3FlatFileReader("<awsKey>","<awsSecretKey>","<awsRegion>","<bucketName>",<FileName>","||");
                 
                //With File Name from AWS S3 Bucket, Separator & Decoding
                reltioFileReader = new ReltioS3FlatFileReader("<awsKey>","<awsSecretKey>","<awsRegion>","<bucketName>",<FileName>","||", "UTF-8");
         
        //Step 3: Read data from the File.
        //It always returns the parsed data. So no need to any parsing of data.
        String[] lineValues = reltioFileReader.readLine();
         
        //Step 4: If lineValues is null then it means reached the End of File.
        while((lineValues = reltioFileReader.readLine()) != null) {
            //Do required opertaion
        }
         
         
        //Step 5: Close the stream
        reltioFileReader.close();
         
    }
}
```

#ReltioFileWriter Interface
As like Reader, this interface also has 4 implementing classes for CSV writer & Flat File Writer for Local storage and AWS S3 Storage
Find the corresponding class details and how to use the core project for writing to a file below:

```java
package com.reltio.file;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
public class SimpleFileWriterTest {
    public static void main(String[] args) throws IOException {
        //Step 1: Create Reltio File writer Reference
        ReltioFileWriter reltioFileWriter;
         
        //Step 2: Create a object based on the File Type.
        //if CSV then use below option
            //Step 2.1: Only with File Name using ReltioCSVFileWriter
            reltioFileWriter = new ReltioCSVFileWriter("<FileName>");
             
            //With File Name & Encoding technique like UTF-8 or any other
            reltioFileWriter = new ReltioCSVFileWriter("<FileName>", "UTF-8");
            
            //Only with AWS S3 File Name using ReltioCSVFileWriter
            reltioFileWriter = new ReltioS3CSVFileWriter("<awsKey>","<awsSecretKey>","<awsRegion>","<bucketName>","<FileName>");
             
            //With AWS S3 File Name & Encoding technique like UTF-8 or any other
            reltioFileWriter = new ReltioS3CSVFileWriter("<awsKey>","<awsSecretKey>","<awsRegion>","<bucketName>","<FileName>", "UTF-8");
         
        //ELSE if FLAT FILE then use below option
            //Step 2.1: Only with File Name. Default Delimiter is pipe
            reltioFileWriter = new ReltioFlatFileWriter("<FileName>");
             
            //With File Name & Encoding
            reltioFileWriter = new ReltioFlatFileWriter("<FileName>","UTF-8");
             
            //With File Name, Separator & Encoding
            reltioFileWriter = new ReltioFlatFileWriter("<FileName>","||", "UTF-8");
            
            //Only with AWS S3 File Name. Default Delimiter is pipe
            reltioFileWriter = new ReltioS3FlatFileWriter("<awsKey>","<awsSecretKey>","<awsRegion>","<bucketName>","<FileName>");
             
            //With AWS S3 File Name & Encoding
            reltioFileWriter = new ReltioS3FlatFileWriter("<awsKey>","<awsSecretKey>","<awsRegion>","<bucketName>","<FileName>","UTF-8");
             
            //With AWS S3 File Name, Separator & Encoding
            reltioFileWriter = new ReltioS3FlatFileWriter("<awsKey>","<awsSecretKey>","<awsRegion>","<bucketName>","<FileName>","||", "UTF-8");
     
    //Step 3: Write data to the File.
            //Step 3.1 Write line by line
            String[] line = null; //only one line
            reltioFileWriter.writeToFile(line);
             
            //Write Bulk of lines to the file
            List<String[]> lines = new ArrayList<>();
            reltioFileWriter.writeToFile(lines);
             
            //Write String data to the file (NOTE: only for Reltio Flat File writter)
            reltioFileWriter.writeToFile("<Flat File>");
             
            //Write Bulk of String line to the file (NOTE: only for Reltio Flat File writter)
            List<String> linesOfString = new ArrayList<>();
            linesOfString.add("<Flat File>");
            reltioFileWriter.writeBulkToFile(linesOfString);
             
     
     
    //Step 4: Close the stream for writting all the lines in the memory
    reltioFileWriter.close();
         
    }
}
```

#Reltio Utils
There are some useful utils within reltio-core-cst that would be used for different projects.
The Password encryption is being implemented in reltio-core-cst
the function is 
com.reltio.cst.util.Util#getProperties(java.lang.String, java.lang.String...)
In each tool the usage should be like the following.
```java
import com.reltio.cst.util.Util;

import java.util.Properties;

class Sample {
    public static void main(String[] args) throws Exception {
        Properties properties;


        properties = Util.getProperties(args[0], "PASSWORD", "DNBPASSWORD");
        properties.getProperty("PASSWORD");//this would be returned a plain password after decryption

    }
}
```


The second parameter could be n number of Properties that would need to be encrypted.
This ensure that that encrypted value is decoded and provided to the code and If the properties is not encrypted then it would encrypt it.


Another util method which lists the mising properties.

```java
import com.reltio.cst.util.Util;

import java.util.Properties;

class Sample {
    public static void main(String[] args) throws Exception {
        Properties properties;


        properties = Util.getProperties(args[0], "PASSWORD", "CLIENT_CREDENTIALS");


		Map<List<String>, List<String>> mutualExclusiveProps = new HashMap<>();
		mutualExclusiveProps.put(Arrays.asList(new String[]{"PASSWORD","USERNAME"}), Arrays.asList(new String[]{"CLIENT_CREDENTIALS"}));
		List<String> missingKeys = Util.listMissingProperties(config,
				Arrays.asList("ENTITY_TYPE","ENVIRONMENT_URL", "AUTH_URL", "FILE_FORMAT", "ENVIRONMENT_URL", "TENANT_ID", "OUTPUT_FILE","TRANSITIVE_MATCH"), mutualExclusiveProps);

    }
}
```

So here the Util validates if any of the property specified in the list is missing or not. If missing it adds to the missing list. 
And also checks for each entry of mutualExclusiveProps, it check either the key or the value must be present. If both are absent then also add these to missing list.


