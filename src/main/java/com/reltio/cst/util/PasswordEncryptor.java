package com.reltio.cst.util;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;

public class PasswordEncryptor {

    public static String encryptPassword(String plainPassword) {
        StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
        encryptor.setPassword("Reltio");
        return encryptor.encrypt(plainPassword);

    }
}
