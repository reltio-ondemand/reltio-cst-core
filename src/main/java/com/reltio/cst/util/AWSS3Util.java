package com.reltio.cst.util;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;

public class AWSS3Util {

	public static AmazonS3 getS3Client(String awsKey, String awsSecretKey, String awsRegion) {
		if (awsKey != null && awsSecretKey != null) {
			AWSCredentials credentials = new BasicAWSCredentials(
					  awsKey, awsSecretKey
					);
			if (awsRegion != null){
			 return AmazonS3ClientBuilder.standard().withRegion(awsRegion).withCredentials(new AWSStaticCredentialsProvider(credentials)).build();	
			} else {
				return AmazonS3ClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(credentials)).build();
			}
			}else{
		 return (AmazonS3)AmazonS3ClientBuilder.standard().build();
			}
	}
}
