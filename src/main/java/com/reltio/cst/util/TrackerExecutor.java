package com.reltio.cst.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class TrackerExecutor extends Thread {

    private static Logger logger = LoggerFactory.getLogger(TrackerExecutor.class.getName());
    int lastCount = 0;
    volatile boolean exit = false;
    long st = System.currentTimeMillis();

    private AtomicInteger count;

    private long interval = 6000l;

    public void setInterval(long interval) {
        this.interval = interval;
    }

    public void setAtomicInteger(AtomicInteger count) {
        this.count = count;
    }

    @Override
    public void run() {

        while (!exit) {

            try {
                sleep(interval);
            } catch (Exception e) {
            }

            int currentCount = count.get();

            double executed = currentCount - lastCount;

            lastCount = currentCount;

            long totalTimeInSeconds = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - st);

            logger.info("[Performance] Overall OPS = " + (count.get() / totalTimeInSeconds));
            logger.info("[Performance] Current OPS = " + executed / 60);
        }

    }

    public void exitThread() {
        exit = true;
    }

}
