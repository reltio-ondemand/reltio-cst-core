package com.reltio.cst.service.impl;

import com.reltio.cst.domain.AuthenticationResponse;
import com.reltio.cst.exception.handler.APICallFailureException;
import com.reltio.cst.exception.handler.GenericException;
import com.reltio.cst.properties.AuthenticationProperties;
import com.reltio.cst.service.RestAPIService;
import com.reltio.cst.service.TokenGeneratorService;
import com.reltio.cst.util.GenericUtilityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import static com.reltio.cst.properties.AuthenticationProperties.INVALID_REFRESH_TOKEN_ERROR;
import static com.reltio.cst.properties.AuthenticationProperties.INVALID_USER_CREDENTIALS;

/**
 * This class implements way to get the token from the Auth server
 */
public class TokenGeneratorServiceImpl extends Thread implements
        TokenGeneratorService {
    private static Logger logger = LoggerFactory.getLogger(TokenGeneratorServiceImpl.class.getName());


    private String username;
    private String password;
    private String authURL;
    private String clientCredentials;
    private Map<String, String> authHeaders = new HashMap<>();
    private AuthenticationResponse authenticationResponse;
    private RestAPIService apiService = new SimpleRestAPIServiceImpl();
    private boolean isRunning;
    private boolean isClientCredentialsAuth;

    /**
     * This constructor will store the user details and throws exception if it
     * is invalid;
     *
     * @param username
     * @param password
     * @param authURL
     * @throws APICallFailureException
     * @throws GenericException
     */
    public TokenGeneratorServiceImpl(
            String username,
            String password,
            String authURL
    ) throws APICallFailureException, GenericException {
        this.username = username;
        this.password = password;
        if (GenericUtilityService.checkNullOrEmpty(authURL)) {
            this.authURL = AuthenticationProperties.DEFAULT_AUTH_SERVER_URL;
        } else {
            this.authURL = authURL;
        }
        setDaemon(true);
        populateAuthHeaders();
        getNewToken();
    }


    /**
     * This constructor will store the user details and throws exception if it
     * is invalid;
     *
     * @param clientCredentials
     * @param authURL
     * @throws APICallFailureException
     * @throws GenericException
     */
    public TokenGeneratorServiceImpl(String clientCredentials,
                                     String authURL
    ) throws APICallFailureException, GenericException {

        isClientCredentialsAuth = true;
        this.clientCredentials = clientCredentials;
        if (GenericUtilityService.checkNullOrEmpty(authURL)) {
            this.authURL = AuthenticationProperties.DEFAULT_AUTH_SERVER_URL;
        } else {
            this.authURL = authURL;
        }
        setDaemon(true);
        populateAuthHeaders();
        getNewToken();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.reltio.cst.service.TokenGeneratorService#startBackgroundTokenGenerator
     * ()
     */
    @Override
    @Deprecated
    public boolean startBackgroundTokenGenerator() {
        if (!isRunning) {
            isRunning = true;
            this.start();
            return true;
        }

        return false;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.reltio.cst.service.TokenGeneratorService#getToken()
     */
    @Override
    public String getToken() throws APICallFailureException, GenericException {
        if (authenticationResponse == null) {
            getNewToken();
        }
        return authenticationResponse.getAccessToken();
    }

    @Override
    public String getRefreshToken() throws APICallFailureException, GenericException {
        if (authenticationResponse == null) {
            getNewToken();
        }
        return authenticationResponse.getRefreshToken();
    }

    /*
     * (non-Javadoc)
     *
     * @see com.reltio.cst.service.TokenGeneratorService#getNewToken()
     */
    @Override
    public String getNewToken() throws APICallFailureException,
            GenericException {
        String responseStr;
        if (isClientCredentialsAuth()) {
            responseStr = getAccessTokenByClientCredentials(authURL, getClientCredentials(), 1);
            authenticationResponse = AuthenticationProperties.GSON.fromJson(responseStr, AuthenticationResponse.class);
        } else if (authenticationResponse == null) {
            responseStr = getAccessToken(authURL, username, password, 1);
            authenticationResponse = AuthenticationProperties.GSON.fromJson(responseStr, AuthenticationResponse.class);
        } else {
            refreshToken();
        }
        return authenticationResponse.getAccessToken();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.reltio.cst.service.TokenGeneratorService#stopBackgroundTokenGenerator
     * ()
     */
    @Override
    @Deprecated
    public boolean stopBackgroundTokenGenerator() {

        if (isRunning && this.isAlive()) {
            logger.info("Background Token Generation process Stopped...");
            this.interrupt();
            return true;
        }
        return false;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Thread#run()
     */
    @Override
    public void run() {

        logger.info("Background Token Generation process Started...");

        // Pre-validation of authentication object
        if (authenticationResponse == null) {
            try {
                refreshToken();
            } catch (APICallFailureException | GenericException e) {
                e.printStackTrace();
            }
        }

        // Infinite loop to run the thread in background for updating the token
        while (!Thread.currentThread().isInterrupted()) {
            try {
                // Wait for 4 mins
                Thread.sleep(240000);
                refreshToken();
            } catch (APICallFailureException | GenericException e) {
                e.printStackTrace();
            } catch (InterruptedException ignored) {
            }
        }
    }

    /**
     * @throws APICallFailureException
     * @throws GenericException
     */
    void refreshToken() throws APICallFailureException,
            GenericException {
        if (authenticationResponse == null || isClientCredentialsAuth()) {
            /*
             * Refresh token does not exist for client credentials. So getting new token if Client Credentials is present
             */
            getNewToken();
        }

        String responseStr = getAccessTokenByRefreshToken(authURL, authenticationResponse.getRefreshToken(), 1);
        authenticationResponse = AuthenticationProperties.GSON.fromJson(responseStr, AuthenticationResponse.class);
    }

    private String getAccessToken(String url, String username, String password, int retryCount) throws APICallFailureException, GenericException {
        try {
            return doAuthAPICall(url, "grant_type=password&username=" + URLEncoder.encode(username, StandardCharsets.UTF_8.name()) + "&password=" + URLEncoder.encode(password, StandardCharsets.UTF_8.name()), retryCount);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    private String getAccessTokenByClientCredentials(String url, String clientCredenatials, int retryCount) throws APICallFailureException, GenericException {
        try {
            return doAuthAPICall(url, "grant_type=client_credentials", retryCount);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private String getAccessTokenByRefreshToken(String url, String refreshToken, int retryCount) throws APICallFailureException, GenericException {
        String accessToken;
        try {
            logger.info("Getting New Token Using Refresh Token..");
            accessToken = doAuthAPICall(url, "grant_type=refresh_token&refresh_token=" + URLEncoder.encode(refreshToken, StandardCharsets.UTF_8.name()), retryCount);
            if (accessToken.equals(INVALID_REFRESH_TOKEN_ERROR)) {
                logger.info("Invalid Refresh Token.. Will Try again with new refresh token..");
                return getAccessToken(authURL, username, password, 1);
            }
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
        logger.debug(accessToken);
        return accessToken;
    }


    /**
     * @param url
     * @param retryCount
     * @return
     * @throws APICallFailureException
     * @throws GenericException
     */
    private String doAuthAPICall(String url, String body, int retryCount)
            throws APICallFailureException, GenericException {
        try {
            return apiService.post(url, authHeaders, body);
        } catch (APICallFailureException e) {
            logger.error("Auth Call Failed: Error Code = "
                    + e.getErrorCode() + " |||| Error Message: "
                    + e.getErrorResponse());

            switch (e.getErrorCode()) {
                case INVALID_USER_CREDENTIALS: {
                    // Send the Exception if the failure due to invalid user credentials
                    throw e;
                }
                case 401: {
                    if (e.getErrorResponse().contains(INVALID_REFRESH_TOKEN_ERROR)) {
                        return INVALID_REFRESH_TOKEN_ERROR;
                    }
                    if (retryCount < AuthenticationProperties.RETRY_LIMIT) {
                        logger.info("Retrying with new token..");
                        return doAuthAPICall(url, body, ++retryCount);
                    }
                    break;
                }
                case 502: {
                    if (retryCount < AuthenticationProperties.RETRY_LIMIT_FOR_502) {
                        try {
                            long sleepTime = (long) (Math.pow(2, retryCount) - 1) * 1000;
                            logger.info("Retrying in " + sleepTime + " milliseconds..");
                            Thread.sleep(sleepTime);
                            return doAuthAPICall(url, body, ++retryCount);
                        } catch (InterruptedException ex) {
                            logger.error("Unexpected interruption exception.. " + ex.getMessage());
                        }
                    }

                    break;
                }
                case 503: {
                    if (retryCount < AuthenticationProperties.RETRY_LIMIT_FOR_503) {
                        try {
                            long sleepTime = (long) (Math.pow(2, retryCount) - 1) * 1000;
                            logger.info("Retrying in " + sleepTime + " milliseconds..");
                            Thread.sleep(sleepTime);
                            return doAuthAPICall(url, body, ++retryCount);
                        } catch (InterruptedException ex) {
                            logger.error("Unexpected interruption exception.. " + ex.getMessage());
                        }
                    }
                    break;
                }
                case 504: {
                    if (retryCount < AuthenticationProperties.RETRY_LIMIT_FOR_504) {
                        try {
                            long sleepTime = (long) (Math.pow(2, retryCount) - 1) * 1000;
                            logger.info("Retrying in " + sleepTime + " milliseconds..");
                            Thread.sleep(sleepTime);
                            return doAuthAPICall(url, body, ++retryCount);
                        } catch (InterruptedException ex) {
                            logger.error("Unexpected interruption exception.. " + ex.getMessage());
                        }
                    }
                    break;
                }
            }
            throw e;

        } catch (GenericException e) {
            logger.error("Auth Call Failed Due to unexpected Exception: Error Message: " + e.getExceptionMessage());

            // Retry till count reaches to expected Retry limit
            if (retryCount < AuthenticationProperties.RETRY_LIMIT) {
                return doAuthAPICall(url, body, ++retryCount);
            }

            throw e;
        }
    }

    /**
     * This is util method to populate the required headers for get token
     */
    private void populateAuthHeaders() {
        if (!isClientCredentialsAuth()) {
            authHeaders.put(AuthenticationProperties.AUTH_SERVER_HEADER, AuthenticationProperties.AUTH_SERVER_BASIC_TOKEN);
        } else {
            authHeaders.put(AuthenticationProperties.AUTH_SERVER_HEADER, AuthenticationProperties.AUTH_HEADER_BASIC_STR + getClientCredentials());
        }

        authHeaders.put(AuthenticationProperties.CONTENT_TYPE_HEADER, "application/x-www-form-urlencoded");


    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the authURL
     */
    public String getAuthURL() {
        return authURL;
    }

    /**
     * @param authURL the authURL to set
     */
    public void setAuthURL(String authURL) {
        this.authURL = authURL;
    }


    /**
     * @return the clientCredentials
     */
    public String getClientCredentials() {
        return clientCredentials;
    }


    /**
     * @param clientCredentials the clientCredentials to set
     */
    public void setClientCredentials(String clientCredentials) {
        this.clientCredentials = clientCredentials;
    }


    /**
     * @return the isClientCredentialsAuth
     */
    public boolean isClientCredentialsAuth() {
        return isClientCredentialsAuth;
    }


    /**
     * @param isClientCredentialsAuth the isClientCredentialsAuth to set
     */
    public void setClientCredentialsAuth(boolean isClientCredentialsAuth) {
        this.isClientCredentialsAuth = isClientCredentialsAuth;
    }

}
