package com.reltio.cst.service;

import java.util.Map;

import com.reltio.cst.domain.HttpMethod;
import com.reltio.cst.exception.handler.GenericException;
import com.reltio.cst.exception.handler.ReltioAPICallFailureException;

/**
 * 
 * This is interface for accessing the Reltio API service which supports list of
 * httpmethods mentioned on this enum <code>{@link HttpMethod}</code>
 * 
 *
 */
public interface ReltioAPIService {

	/**
	 * Helps to do the Reltio GET API calls
	 * @param requestUrl - URL of the API call with all Query parameters (no encoding required)
	 * @param requestHeaders - Key/Value pair of expected Request Headers. (No need to add Reltio token & content-type headers)
	 * @return
	 * @throws GenericException
	 * @throws ReltioAPICallFailureException
	 */
	public String get(String requestUrl, Map<String, String> requestHeaders)
			throws GenericException, ReltioAPICallFailureException;

	/**
	 * 
	 * Helps to do the Reltio POST API calls
	 * @param requestUrl - URL of the API call with all Query parameters (no encoding required)
	 * @param requestHeaders - Key/Value pair of expected Request Headers. (No need to add Reltio token & content-type headers)
	 * @param requestBody- JSON Request body or null if nothing send in the body
	 * @return
	 * @throws GenericException
	 * @throws ReltioAPICallFailureException
	 */
	public String post(String requestUrl, Map<String, String> requestHeaders,
			String requestBody) throws GenericException,
			ReltioAPICallFailureException;

	/**
	 * 
	 * Helps to do the Reltio PUT API calls
	 * 
	 * @param requestUrl - URL of the API call with all Query parameters (no encoding required)
	 * @param requestHeaders - Key/Value pair of expected Request Headers. (No need to add Reltio token & content-type headers)
	 * @param requestBody- JSON Request body or null if nothing send in the body
	 * @return
	 * @throws GenericException
	 * @throws ReltioAPICallFailureException
	 */
	public String put(String requestUrl, Map<String, String> requestHeaders,
			String requestBody) throws GenericException,
			ReltioAPICallFailureException;

	/**
	 * Helps to do the Reltio DELETE API calls
	 * 
	 * @param requestUrl - URL of the API call with all Query parameters (no encoding required)
	 * @param requestHeaders - Key/Value pair of expected Request Headers. (No need to add Reltio token & content-type headers)
	 * @param requestBody- JSON Request body or null if nothing send in the body
	 * @return
	 * @throws GenericException
	 * @throws ReltioAPICallFailureException
	 */
	public String delete(String requestUrl, Map<String, String> requestHeaders,
			String requestBody) throws GenericException,
			ReltioAPICallFailureException;

	/**
	 * Generic Method to any Reltio API call listed in <code>{@link HttpMethod}</code>
	 * 
	 * @param requestUrl - URL of the API call with all Query parameters (no encoding required)
	 * @param requestHeaders - Key/Value pair of expected Request Headers. (No need to add Reltio token & content-type headers)
	 * @param requestBody- JSON Request body or null if nothing send in the body
	 * @param requestMethod - USE the enum <code>{@link HttpMethod}</code> to select the required http method
	 * @return
	 * @throws GenericException
	 * @throws ReltioAPICallFailureException
	 */
	public String doExecute(String requestUrl,
			Map<String, String> requestHeaders, String requestBody,
			HttpMethod requestMethod) throws GenericException,
			ReltioAPICallFailureException;

	/**
	 * Helps to do the Reltio GET API calls with less method params
	 * 
	 * @param requestUrl - URL of the API call with all Query parameters (no encoding required)
	 * @return
	 * @throws GenericException
	 * @throws ReltioAPICallFailureException
	 */
	public String get(String requestUrl) throws GenericException,
			ReltioAPICallFailureException;

	/**
	 * Helps to do the Reltio POST API calls with less method params
	 * 
	 * @param requestUrl - URL of the API call with all Query parameters (no encoding required)
	 * @param requestBody- JSON Request body or null if nothing send in the body
	 * @return
	 * @throws GenericException
	 * @throws ReltioAPICallFailureException
	 */
	public String post(String requestUrl, String requestBody)
			throws GenericException, ReltioAPICallFailureException;

	/**
	 * 
	 * Helps to do the Reltio PUT API calls with less method params
	 *  
	 * @param requestUrl - URL of the API call with all Query parameters (no encoding required)
	 * @param requestBody- JSON Request body or null if nothing send in the body
	 * @return
	 * @throws GenericException
	 * @throws ReltioAPICallFailureException
	 */
	public String put(String requestUrl, String requestBody)
			throws GenericException, ReltioAPICallFailureException;

	/**
	 * 
	 * Helps to do the Reltio DELETE API calls with less method params
	 * 
	 * @param requestUrl - URL of the API call with all Query parameters (no encoding required)
	 * @param requestBody - JSON Request body or null if nothing send in the body
	 * @return
	 * @throws GenericException
	 * @throws ReltioAPICallFailureException
	 */
	public String delete(String requestUrl, String requestBody)
			throws GenericException, ReltioAPICallFailureException;

}
