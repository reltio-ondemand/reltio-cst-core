package com.reltio.cst.exception.handler;

/**
 * 
 * This is generic Exception for all API call Failures
 * 
 *
 */
public class APICallFailureException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4779839126777615195L;

	private Integer errorCode;
	private String errorResponse;

	public Integer getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorResponse() {
		return errorResponse;
	}

	public void setErrorResponse(String errorResponse) {
		this.errorResponse = errorResponse;
	}

	public APICallFailureException(Integer errorCode, String errorResponse) {

		this.errorCode = errorCode;
		this.errorResponse = errorResponse;
	}

}
