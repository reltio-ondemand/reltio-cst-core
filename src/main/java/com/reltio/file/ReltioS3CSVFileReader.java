package com.reltio.file;

import au.com.bytecode.opencsv.CSVReader;
import org.apache.tika.parser.txt.CharsetDetector;
import org.apache.tika.parser.txt.CharsetMatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.reltio.cst.util.AWSS3Util;

import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CodingErrorAction;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;

/**
 * @author Mallikarjuna.Aakulati@reltio.com Created : May 28, 2021
 */
public class ReltioS3CSVFileReader implements ReltioFileReader, AutoCloseable {
    private static Logger logger = LoggerFactory.getLogger(ReltioS3CSVFileReader.class.getName());


    private CSVReader csvReader = null;
    private S3Object s3object = null;
    Exception caughtException = null;

    public ReltioS3CSVFileReader(String awsKey, String awsSecretKey, String awsRegion, String bucket, String fileName) throws IOException {
    	
    	try {
    		AmazonS3 s3Client = null;
    		s3Client=AWSS3Util.getS3Client(awsKey, awsSecretKey, awsRegion);
    		s3object = s3Client.getObject(bucket, fileName);
    		S3ObjectInputStream inputStream = s3object.getObjectContent();
    	
    		BufferedInputStream isr = new BufferedInputStream(s3object.getObjectContent());

	        CharsetDetector charsetDetector = new CharsetDetector();
	        charsetDetector.setText(isr);
	        charsetDetector.enableInputFilter(true);
	        CharsetMatch cm = charsetDetector.detect();
	        logger.info("Decorder of the File (CharSet) :: " + cm.getName());
	        isr.close();
	        BufferedReader fileReader = null;
	        if (Charset.availableCharsets().get(cm.getName()) == null) {
	            logger.info("The "
	                    + cm.getName()
	                    + " charset not supported. So letting the reader to choose apporiate the Charset...");
	            fileReader = new BufferedReader(new InputStreamReader(inputStream));
	
	        } else {
	            CharsetDecoder newdecoder = Charset.forName(cm.getName())
	                    .newDecoder();
	            newdecoder.onMalformedInput(CodingErrorAction.REPLACE);
	            fileReader = new BufferedReader(
	            		new InputStreamReader(inputStream, newdecoder));
	
	        }
	        readBOMMarker(fileReader);
	        csvReader = new CSVReader(fileReader);
    	}catch (AmazonServiceException e) {
            // The call was transmitted successfully, but Amazon S3 couldn't process 
            // it, so it returned an error response.
    		caughtException = e;
    		logger.error("Amazon S3 CSV Service Exception :: ");
			logger.error(e.getMessage());
            e.printStackTrace();
        }catch (SdkClientException e) {
            // Amazon S3 couldn't be contacted for a response, or the client
            // couldn't parse the response from Amazon S3.
        	caughtException = e;
        	logger.error("Amazon S3 CSV SDK Client exception :: ");
			logger.error(e.getMessage());
            e.printStackTrace();
        }finally {
            // To ensure that the network connection doesn't remain open, close any open input streams.
        	if (caughtException != null) {
        	logger.error("Exeption in Reltio S3 CSV Read Program :: ");
			logger.error(caughtException.getMessage());
        	}
        }
    }

    public ReltioS3CSVFileReader(String awsKey, String awsSecretKey, String awsRegion, String bucket, String fileName, String decoder)
            throws FileNotFoundException, IOException {
    	
    	try {
    		AmazonS3 s3Client = null;
    		s3Client=AWSS3Util.getS3Client(awsKey, awsSecretKey, awsRegion);
	    	s3object = s3Client.getObject(bucket, fileName);
	    	S3ObjectInputStream inputStream = s3object.getObjectContent();
	    	
	        CharsetDecoder newdecoder = Charset.forName(decoder).newDecoder();
	        newdecoder.onMalformedInput(CodingErrorAction.REPLACE);
	        BufferedReader fileReader = new BufferedReader(new InputStreamReader(inputStream,
	                newdecoder));
	        readBOMMarker(fileReader);
	        csvReader = new CSVReader(fileReader);
    	}catch (AmazonServiceException e) {
            // The call was transmitted successfully, but Amazon S3 couldn't process 
            // it, so it returned an error response.
    		caughtException = e;
    		logger.error("Amazon S3 CSV Service exception :: ");
			logger.error(e.getMessage());
            e.printStackTrace();
        }catch (SdkClientException e) {
            // Amazon S3 couldn't be contacted for a response, or the client
            // couldn't parse the response from Amazon S3.
        	caughtException = e;
        	logger.error("Amazon S3 CSV SDK Client exception :: ");
			logger.error(e.getMessage());
            e.printStackTrace();
        }finally {
            // To ensure that the network connection doesn't remain open, close any open input streams.
        	if (caughtException != null) {
            	logger.error("Exeption in Reltio S3 CSV Read Program :: ");
    			logger.error(caughtException.getMessage());
            	}
        }
    }

    @Override
    public String[] readLine() throws IOException {
        return csvReader.readNext();
    }

    @Override
    public void close() throws IOException {
        csvReader.close();
    }

    private void readBOMMarker(BufferedReader fileReader) {
        try {
            fileReader.mark(4);
            if ('\ufeff' != fileReader.read())
                fileReader.reset(); // not the BOM marker
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
