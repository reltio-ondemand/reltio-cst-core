package com.reltio.file;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.reltio.cst.util.AWSS3Util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.CodingErrorAction;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Mallikarjuna.Aakulati@reltio.com Created : May 28, 2021
 */
public class ReltioS3FlatFileWriter implements ReltioFileWriter, AutoCloseable {
    private static Logger logger = LoggerFactory.getLogger(ReltioS3FlatFileWriter.class.getName());

    public static final int INITIAL_STRING_SIZE = 128;
    private static final String lineEnd = System.lineSeparator();

    private String separator = "|";

    private CharsetEncoder encoder = Charset.forName("UTF-8").newEncoder();

    private final ByteArrayOutputStream stream;
    private final OutputStreamWriter streamWriter;
    private final AmazonS3 s3Client;
    private String bucket;
    private String fileName;
    private String encoding="UTF-8";
    
    /**
     * By default UTF-8 encoding used and separator will be pipe
     *
     * @param fileName
     * @throws IOException
     */
    public ReltioS3FlatFileWriter(String bucket, String fileName, String awsKey, String awsSecretKey, String awsRegion) throws IOException {
        encoder.onMalformedInput(CodingErrorAction.REPORT);
        encoder.onUnmappableCharacter(CodingErrorAction.REPORT);
        
        stream = new ByteArrayOutputStream();
        streamWriter = new OutputStreamWriter(stream);
        
		this.s3Client=AWSS3Util.getS3Client(awsKey, awsSecretKey, awsRegion);
        this.bucket = bucket;
        this.fileName = fileName;
    }

    public ReltioS3FlatFileWriter(String bucket, String fileName, String awsKey, String awsSecretKey, String awsRegion, String encoding) throws IOException {
        encoder.onMalformedInput(CodingErrorAction.REPORT);
        encoder.onUnmappableCharacter(CodingErrorAction.REPORT);
        
        stream = new ByteArrayOutputStream();
        streamWriter = new OutputStreamWriter(stream);
        
        this.s3Client=AWSS3Util.getS3Client(awsKey, awsSecretKey, awsRegion);
        this.bucket = bucket;
        this.fileName = fileName;
        this.encoding=encoding;
        encoder = Charset.forName(encoding).newEncoder();
    }

    public ReltioS3FlatFileWriter(String bucket, String fileName, String awsKey, String awsSecretKey, String awsRegion, String encoding, String separator) throws IOException {
        encoder.onMalformedInput(CodingErrorAction.REPORT);
        encoder.onUnmappableCharacter(CodingErrorAction.REPORT);
        
        stream = new ByteArrayOutputStream();
        streamWriter = new OutputStreamWriter(stream);
        
        this.s3Client=AWSS3Util.getS3Client(awsKey, awsSecretKey, awsRegion);
        this.bucket = bucket;
        this.fileName = fileName;
        this.separator = separator;
        this.encoding=encoding;
        encoder = Charset.forName(encoding).newEncoder();
    }

    public void writeToFile(List<String[]> lines) throws IOException {
    	if (lines != null) {
            synchronized (streamWriter) {
                for (String[] line : lines) {
                    writeDataToFile(line);
                }
                streamWriter.flush();
            }
        }
    }

    public void close() throws IOException {
    	streamWriter.flush();
    	if (!this.fileName.endsWith("_RejectedRecords.txt") && !stream.toString().isEmpty()){
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentType("text/html");
            metadata.setContentEncoding(encoding);
            metadata.setContentLength(stream.toByteArray().length);
            streamWriter.close();
            s3Client.putObject(this.bucket, this.fileName, new ByteArrayInputStream(stream.toByteArray()), metadata);
    		
    	}
    	if (StringUtils.countMatches(stream.toString(), lineEnd)>1 && this.fileName.endsWith("_RejectedRecords.txt")){
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentType("text/html");
            metadata.setContentEncoding(encoding);
            metadata.setContentLength(stream.toByteArray().length);
            streamWriter.close();
            s3Client.putObject(this.bucket, this.fileName, new ByteArrayInputStream(stream.toByteArray()), metadata);
    	}
    }

    /**
     * Writes the next line to the file.
     *
     * @param nextLine a string array with each comma-separated element as a separate
     *                 entry.
     * @throws IOException
     */
    public void writeToFile(String[] nextLine) throws IOException {
    	synchronized (streamWriter) {
            writeDataToFile(nextLine);
            streamWriter.flush();
        }
    }
    
    private void writeDataToFile(String[] nextLine) throws IOException {
        if (nextLine == null)
            return;
        StringBuilder sb = new StringBuilder(INITIAL_STRING_SIZE);
        for (int i = 0; i < nextLine.length; i++) {

            if (i != 0) {
                sb.append(separator);
            }

            String nextElement = nextLine[i];
            if (nextElement == null)
                continue;
            sb.append(nextElement);
        }
        
        sb.append(lineEnd);
        streamWriter.write(sb.toString());
    }

    @Override
    public void writeBulkToFile(List<String> lines) throws IOException {
    	if (lines != null && !lines.isEmpty()) {
            synchronized (streamWriter) {
                for (String line : lines) {
                	streamWriter.write(line + lineEnd);
                }
                streamWriter.flush();
            }
        }
    }

    @Override
    public void writeToFile(String line) throws IOException {
    	if (line != null) {
            synchronized (streamWriter) {
            	streamWriter.write(line + lineEnd);
            }
            streamWriter.flush();
        }
    }

    /**
     * @param separator the separator to set
     */
    public void setSeparator(String separator) {
        this.separator = separator;
    }
}
